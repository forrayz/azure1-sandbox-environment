#!/usr/bin/env groovy
pipeline {
    agent {
        docker { image 'hashicorp/terraform:1.1.7' }
    }

    parameters {
        string(name: 'CLIENT', defaultValue: 'customer1', description: 'Cloud provider')
        string(name: 'ENV', defaultValue: 'development', description: 'Environment')
        string(name: 'INFRA', defaultValue: 'azure', description: 'Cloud provider')
    }

    environment {
        GCP_PROJECT_NAME = 'azure1-sandbox-environment'
        DEBIAN_FRONTEND = 'noninteractive'
        //Secret File ID was defined in Jenkins -> Credentials -> System -> Global credentials
        AWS_ACCESS_KEY_ID = credentials('AWS_ACCESS_KEY_ID')
        AWS_SECRET_ACCESS_KEY = credentials('AWS_SECRET_ACCESS_KEY')
        GOOGLE_APPLICATION_CREDENTIALS = credentials('GOOGLE_APPLICATION_CREDENTIALS')
        ARM_CLIENT_ID = credentials('ARM_CLIENT_ID')
        ARM_CLIENT_SECRET = credentials('ARM_CLIENT_SECRET')
        ARM_SUBSCRIPTION_ID = credentials('ARM_SUBSCRIPTION_ID')
        ARM_TENANT_ID = credentials('ARM_TENANT_ID')
        DOCKERHUB_USER = credentials('DOCKERHUB_USER')
        DOCKERHUB_PASSWORD = credentials('DOCKERHUB_PASSWORD')
        GCP_AUTH_JSON = credentials('GCP_AUTH_JSON')
        gcp_client_id = credentials('gcp_client_id')
        gcp_private_key = credentials('gcp_private_key')
        PRIVATE_TOKEN = credentials('PRIVATE_TOKEN')
        TF_ssh_key = credentials('TF_ssh_key')
        TF_ssh_pubkey = credentials('TF_ssh_pubkey')
        //gitlab CI Variables
        CI_API_V4_URL = 'https://gitlab.com/api/v4'
        ARTIFACT_PROJECT_ID = '34545076'
    }

    stages {
        stage('context-init') {
            agent {
                docker { image 'python:3.7-alpine' }
            }
            steps {
                sh '''
                pip --no-cache-dir install --upgrade pip ; pip --no-cache-dir install j2cli[yaml]==0.3.10
                sh echo "INFRA---->${params.INFRA}  CLIENT---->${params.CLIENT}  ENV---->${params.ENV}  INFRA--->${params.INFRA}"
                sh mv -v terraform_${params.INFRA} ${params.CLIENT}-${params.ENV}-${params.INFRA} 
                sh j2 ${params.CLIENT}-${params.ENV}-${params.INFRA}/variables.tf.j2 | tee ${params.CLIENT}-${params.ENV}-${params.INFRA}/variables.tf
                sh j2 ${params.CLIENT}-${params.ENV}-${params.INFRA}/backend.tf.j2   | tee ${params.CLIENT}-${params.ENV}-${params.INFRA}/backend.tf
                sh j2 ${params.CLIENT}-${params.ENV}-${params.INFRA}/main.tf.j2      | tee ${params.CLIENT}-${params.ENV}-${params.INFRA}/main.tf
                sh j2 ${params.CLIENT}-${params.ENV}-${params.INFRA}/tomcat.tf.j2    | tee ${params.CLIENT}-${params.ENV}-${params.INFRA}/tomcat.tf
                sh j2 ${params.CLIENT}-${params.ENV}-${params.INFRA}/postgres.tf.j2  | tee ${params.CLIENT}-${params.ENV}-${params.INFRA}/postgres.t
                '''
            }
        }

        stage('yamlint') {
            agent {
                docker { image 'sdesbure/yamllint' }
            }
            steps {
                sh "yamllint ${params.CLIENT}-${params.ENV}-${params.INFRA}/"
            }
        }

        stage('fmt-check') {
            steps {
                sh "terraform fmt -recursive -diff -write=false -list=true -check ${params.CLIENT}-${params.ENV}-${params.INFRA}"
            }
        }
        stage('tflint') {
            agent {
                docker { image 'ghcr.io/terraform-linters/tflint:latest' }
            }
            steps {
                sh "tflint ${params.CLIENT}-${params.ENV}-${params.INFRA}"
            }
        }
        stage('test') {
            steps {
                sh '''
                cd ${params.CLIENT}-${params.ENV}-${params.INFRA}
                pwd ; ls -lah ; terraform version
                terraform init  
                terraform validate
                cd .. ; pwd
                '''
            }
        }
        stage('plan-apply') {
            steps {
                sh '''
                        cd ${params.CLIENT}-${params.ENV}-${params.INFRA}
                        terraform init
                        terraform plan -out out.tfplan
                        terraform apply -input=false out.tfplan
                        cd ..
                        touch ansible/inventory/hosts.cfg # <------since GCP is not done yet lets fool this here ;)
                        pwd
                        mv -v ansible/inventory/hosts.cfg  ansible/inventory/${params.CLIENT}-${params.ENV}-${params.INFRA}-inventory.cfg
                        '''
            }
        }

        stage('ping') {
                agent {
                    docker { image 'ubuntu' }
                }
                steps {
                    sh '''
                    # Setup SSH credentials and known host
                    which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )
                    mkdir -p ~/.ssh
                    echo ${env.TF_ssh_key} | tr -d '\r' > ~/.ssh/id_rsa
                    chmod 700 ~/.ssh/id_rsa
                    eval "$(ssh-agent -s)"
                    ssh-add ~/.ssh/id_rsa
                    apt update -y
                    apt install -y software-properties-common
                    add-apt-repository --yes --update ppa:ansible/ansible
                    apt -y install ansible
                    ansible -i ansible/inventory/${params.CLIENT}-${params.ENV}-${params.INFRA}-inventory.cfg -m ping all
                    '''
                }
            }

            stage('play') {
                agent {
                    docker { image 'ubuntu' }
                }
                steps {
                    sh '''
                apt update -y ;apt install -y curl zip unzip
                which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )
                mkdir -p ~/.ssh
                echo ${env.TF_ssh_key} | tr -d '\\r' > ~/.ssh/id_rsa
                chmod 700 ~/.ssh/id_rsa
                eval "$(ssh-agent -s)"
                ssh-add ~/.ssh/id_rsa
                apt update -y ; apt install -y software-properties-common ;  add-apt-repository --yes --update ppa:ansible/ansible ; apt -y install ansible
                export ARTIFACT_JOB_URL=${env.CI_API_V4_URL}/projects/${env.ARTIFACT_PROJECT_ID}/jobs/artifacts/${env.BRANCH_NAME}/download?job=${ARTIFACT_JOB_NAME}
                echo "ARTIFACT_PROJECT_ID-----> $ARTIFACT_PROJECT_ID" ; echo "ARTIFACT_JOB_URL--------> $ARTIFACT_JOB_URL"
                echo "CLIENT----------->${CLIENT}"
                echo "ENV-------------->${ENV}"
                echo "PRIVATE-TOKEN---> $PRIVATE_TOKEN ARTIFACT_JOB_URL---> $ARTIFACT_JOB_URL"
                curl -L -v --fail --header "PRIVATE-TOKEN: ${env.PRIVATE_TOKEN}" "${ARTIFACT_JOB_URL}" --output ansible/artifacts.zip
                cd ansible
                zip -T artifacts.zip
                unzip artifacts.zip 
                ls artifacts 
                mv artifacts/*.war  artifacts/app.war ; cd ..
                # play
                mv ansible/inventory/${params.CLIENT}-${params.ENV}-${params.INFRA}-inventory.cfg ansible/inventory/hosts
                rm -fv ansible/inventory/*.cfg
                pwd ;ansible-playbook -i ansible/inventory/ --private-key ~/.ssh/id_rsa ansible/site.yml
                '''
                }
            }

            stage('destroy') {
                steps {
                    sh '''
                cd ${CLIENT}-${ENV}-${INFRA}
                terraform init
                terraform destroy -auto-aproove
                cd ..
                echo done
                '''
                }
            }
        }

        post {
            always {
                archiveArtifacts artifacts: 'tfplan.txt'
            }
        }
    }
