# azure1-sandbox-environment
## Code delivery with gitlab --> terraform --> ansible

## namespace
CLIENT.ENV.INFRA


## prepare Azure

``` ./scripts/install-azure-cli.sh ```



### login wit az-cli

```az login```


### set account

``` az account set --subscription "5f5d2c32-9a0c-4e60-8493-78458d817b59" ```

### create service principal azure

```bash 
az ad sp create-for-rbac --role="Contributor" --scopes="/subscriptions/5f5d2c32-9a0c-4e60-8493-78458d817b59"

Creating 'Contributor' role assignment under scope '/subscriptions/35akss-subscription-id'
The output includes credentials that you must protect. Be sure that you do not include these credentials in your code or check the credentials into your source control. For more information, see https://aka.ms/azadsp-cli
{
  "appId": "xxxxxx-xxx-xxxx-xxxx-xxxxxxxxxx",
  "displayName": "azure-cli-2022-xxxx",
  "password": "xxxxxx~xxxxxx~xxxxx",
  "tenant": "xxxxx-xxxx-xxxxx-xxxx-xxxxx"
}
 ```

###  Create gitlab CI/CD variables shown below. Also for local testing create a .secret file with the following content:

```bash

$ export ARM_CLIENT_ID="<APPID_VALUE>"
$ export ARM_CLIENT_SECRET="<PASSWORD_VALUE>"
$ export ARM_SUBSCRIPTION_ID="<SUBSCRIPTION_ID>"
$ export ARM_TENANT_ID="<TENANT_VALUE>"

```

## Decode your private and public key to store in gitlab CI/CD variable

```bash
TF_ssh_pubkey
TF_ssh_key
```


## test jobs without gitlab
- plase a .secret file to ther parent folder of this project with the following filled out
```bash
export ARM_CLIENT_ID=?
export ARM_CLIENT_SECRET=?
export ARM_SUBSCRIPTION_ID=?
export ARM_TENANT_ID=?
```
## use scripts/test_jobs.sh for non gitlab testing
Testing the tf code without gitlab Ci fasten the traoubleshooting. The script got all job logic in functions.

## caveats
- jobs plan/play has tu run twice to get ansible inventory file filled with IP addresses.
- instead of ugly cfg inventory format i would like to have an inventory.yaml fiel generated. However terraform adds extra tabs to the file during templating.

### misc
```bash
# test job handle branch
set rebase_branch feature/1.0.0 ;  git checkout $rebase_branch ; git rebase main ; git add --all ; git add . ; git push origin $rebase_branch ; git checkout main

git add . ; git add --all ; git commit -am 'check' ; git push origin main ; set rebase_branch feature/1.0.0 ;  \
git checkout $rebase_branch ; git rebase main ; git add --all ; git add . ; git push origin $rebase_branch ; git checkout main


```bash