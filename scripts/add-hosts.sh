resource "local_file" "host_script" {
    filename = "./add_host.sh"

    content = <<-EOF
    echo "Setting SSH Key"
    ssh-add ~/<PATH TO SSH KEYFILE>.pem
    echo "Adding IPs"

    ssh-keyscan -H ${module.frontend[0].private_ip} >> ~/.ssh/known_hosts
    ssh-keyscan -H ${module.frontend[1].private_ip} >> ~/.ssh/known_hosts
    ssh-keyscan -H ${module.frontend[2].private_ip} >> ~/.ssh/known_hosts
    ssh-keyscan -H ${module.backend[0].private_ip} >> ~/.ssh/known_hosts
    ssh-keyscan -H ${module.backend[1].private_ip} >> ~/.ssh/known_hosts
    ssh-keyscan -H ${module.backend[2].private_ip} >> ~/.ssh/known_hosts
    ssh-keyscan -H ${module.auth_server[0].private_ip} >> ~/.ssh/known_hosts
    ssh-keyscan -H ${module.repository_server[0].private_ip} >> ~/.ssh/known_hosts

    EOF

}