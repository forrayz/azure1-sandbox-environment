#!/usr/bin/env bash
set -o errexit
set -o pipefail
set -o nounset
echo  "╔══════════════════════════════════════════════════════════════════════════════════════════════╗"
echo  "║     install-azure-cli.sh BASH script                                                         ║"
echo  "║     Author        : Zoltán Forray                                                            ║"
echo  "║     version 0.0.0 :                                                                          ║"
echo  "║     proj                                                                                     ║"
echo  "║     based on : https://docs.microsoft.com/en-us/cli/azure/install-azure-cli-linux?pivots=apt ║"
echo  "╚══════════════════════════════════════════════════════════════════════════════════════════════╝"
#trap "set +x; sleep 1; set -x" DEBUG

sudo apt-get update -y
sudo apt-get install ca-certificates curl apt-transport-https lsb-release gnupg -y
curl -sL https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/microsoft.gpg > /dev/null
AZ_REPO=$(lsb_release -cs)
echo "deb [arch=amd64] https://packages.microsoft.com/repos/azure-cli/ $AZ_REPO main" | sudo tee /etc/apt/sources.list.d/azure-cli.list
sudo apt-get update -y
sudo apt-get install azure-cli -y
