#!/usr/bin/env bash
set -o errexit
set -o pipefail
set -o nounset
echo  "╔══════════════════════════════════════════════════════════════════════════════════════════════╗"
echo  "║     install-terraform.sh BASH script                                                         ║"
echo  "║     Author        : Zoltán Forray                                                            ║"
echo  "║     version 0.0.0 :                                                                          ║"
echo  "║                                                                                              ║"
echo  "║     based on : https://www.terraform.io/downloads                                            ║"
echo  "╚══════════════════════════════════════════════════════════════════════════════════════════════╝"
#trap "set +x; sleep 1; set -x" DEBUG
curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
sudo apt-get update -y && sudo apt-get -y install terraform 
