#!/usr/bin/env bash
set -o errexit
set -o pipefail
set -o nounset
echo  "╔════════════════════════════════════════════════════════════╗"
echo  "║     test_jobs.sh BASH scipt                                ║"
echo  "║     Author        : Zoltán Forray                          ║"
echo  "║     version 1.1.0 :                                        ║"
echo  "║     Simulate gitlab jobs. Each function is one job         ║"
echo  "║     keep your secret in a .secret file one folder above    ║"
echo  "╚════════════════════════════════════════════════════════════╝"
# trap "set +x; sleep 1; set -x" DEBUG

source ../.secret
source .env
ENVIRONMENT_PREFIX=${CI_PROJECT_NAME}-${CI_BUILD_REF_SLUG}-${CLIENT}-${ENV}-${INFRA}
export TF_ssh_pubkey=$(cat ~/.ssh/id_rsa.pub)

az_cleanup()
    {
    az resource delete --verbose --ids /subscriptions/${ARM_SUBSCRIPTION_ID}/resourceGroups/${ENVIRONMENT_PREFIX}    
    }

show_context()
    {
        echo "ARM_CLIENT_ID══════════════════════>$ARM_CLIENT_ID"
        echo "ARM_CLIENT_SECRET══════════════════>$ARM_CLIENT_SECRET"
        echo "ARM_SUBSCRIPTION_ID════════════════>$ARM_SUBSCRIPTION_ID"
        echo "ARM_TENANT_ID══════════════════════>$ARM_TENANT_ID"
        echo "CI_PROJECT_NAME════════════════════>$CI_PROJECT_NAME"
        echo "ENV════════════════════════════════>$ENV"
        echo "INFRA══════════════════════════════>$INFRA"
        echo "CLIENT═════════════════════════════>$CLIENT"
        echo "CI_BUILD_REF_SLUG══════════════════>$CI_BUILD_REF_SLUG"
        echo "ENVIRONMENT_PREFIX═════════════════>$ENVIRONMENT_PREFIX"
        echo "TF_ssh_pubkey══════════════════════>$TF_ssh_pubkey"
        echo "GOOGLE_APPLICATION_CREDENTIALS═════>$GOOGLE_APPLICATION_CREDENTIALS"
    }

context_init() {
#context init
 j2 terraform_${INFRA}/variables.tf.j2 | tee terraform_${INFRA}/variables.tf
 j2 terraform_${INFRA}/backend.tf.j2 | tee terraform_${INFRA}/backend.tf
 j2 terraform_${INFRA}/main.tf.j2 | tee terraform_${INFRA}/main.tf
 j2 terraform_${INFRA}/postgres.tf.j2 | tee terraform_${INFRA}/postgres.tf
 j2 terraform_${INFRA}/tomcat.tf.j2 | tee terraform_${INFRA}/tomcat.tf
}

plan()
    {
        cd terraform_${INFRA}
        terraform providers
        terraform init || true
        terraform init -upgrade
        # terraform init
        # if [ $INFRA == "azure" ] ; then terraform import azurerm_resource_group.${CI_PROJECT_NAME} /subscriptions/$ARM_SUBSCRIPTION_ID/resourceGroups/${ENVIRONMENT_PREFIX} || true ; fi
        terraform plan -out out.tfplan
        cd ..
}

apply()
    {
        cd terraform_${INFRA}
        terraform apply -input=false out.tfplan
        cd ..
    }
        
ping()
    {
        ansible -i ansible/inventory/hosts.cfg -m ping all
    }

play()
    {
        ansible-playbook -i ansible/inventory/host.cfg  ansible/site.yml
    }

destroy()
    {
    cd terraform_${INFRA}
    terraform destroy
    ..
    }
show_az_info()
    {
        #az group show --name $CI_PROJECT_NAME
        # az vm list-skus --location germanywestcentral --size B1s --all --output table
        # az vm image list-offers --location germanywestcentral --publisher RedHat
        # az vm list-skus --location germanywestcentral --size Standard_B --all --output table | grep --invert-match NotAvailableForSubscription
        # az account show
        # az account list-locations
        az vm list-skus --location centralus --output table | grep --invert-match NotAvailableForSubscription
    }

#az_cleanup
show_context
context_init
plan
#apply
destroy
ping
play
destroy
# show_az_info
