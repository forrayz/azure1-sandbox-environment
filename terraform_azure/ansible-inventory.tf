# generate inventory file for Ansible
resource "local_file" "hosts_cfg" {
  content = templatefile("${path.module}/hosts.tftpl",
    {
      tomcat_servers   = data.azurerm_public_ip.tomcat.*.ip_address
      postgres_servers = data.azurerm_public_ip.postgres.*.ip_address
    }
  )
  filename = "../ansible/inventory/hosts.cfg"
}
