##############################################################################
# Outputs File
#
# Expose the outputs you want your users to see after a successful 
# `terraform apply` or `terraform output` command. You can add your own text 
# and include any data from the state file. Outputs are sorted alphabetically;
# use an underscore _ to move things to the bottom. 

# output "_instructions" {
#   value = "This output contains plain text. You can add variables too."
# }

# output "postgres_server_IPs" {
#   value = azurerm_public_ip.postgres.fqdn
# }

# output "postgres_server_url" {
#   value = "http://${azurerm_public_ip.postgres.fqdn}"
# }

output "postgres_public_ip_address" {
  value = data.azurerm_public_ip.postgres.*.ip_address
}

output "tomcat_public_ip_address" {
  value = data.azurerm_public_ip.tomcat.*.ip_address
}
