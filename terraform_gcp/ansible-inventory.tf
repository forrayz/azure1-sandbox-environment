# generate inventory file for Ansible
resource "local_file" "hosts_cfg" {
  content = templatefile("${path.module}/hosts.tftpl",
    {
      tomcat_servers   = google_compute_instance.tomcat.network_interface.0.access_config.*.nat_ip
      postgres_servers = google_compute_instance.postgres.network_interface.0.access_config.*.nat_ip
    }
  )
  filename = "../ansible/inventory/hosts.cfg"
}
