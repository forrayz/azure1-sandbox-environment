terraform {
  required_version = "~> 1.1.7"
}

provider "google" {
  project = "azure1-sandbox-environment"
  region  = "us-west1"
}
