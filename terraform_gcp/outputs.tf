output "tomcat_IPs" {
  value = google_compute_instance.tomcat.network_interface.0.access_config.0.nat_ip
}

output "postgres_IPs" {
  value = google_compute_instance.postgres.network_interface.0.access_config.0.nat_ip
}